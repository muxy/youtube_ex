defmodule YoutubeExTest do
  use ExUnit.Case
  use ExVCR.Mock, adapter: ExVCR.Adapter.IBrowse

  setup_all do
    HTTPotion.start
    :ok
  end

  test "a good live comment response is parsed correctly" do
    use_cassette "BIJ9Mfr2mtg-live-comments", match_requests_on: [:query] do
      comments = YoutubeEx.get_video_live_comments("BIJ9Mfr2mtg", 1450818352)

      assert length(comments["comments"]) == 56
      assert comments["latest_time"] == "1450818412"
      assert comments["poll_delay"] == 3000
    end
  end
end
