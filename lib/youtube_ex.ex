defmodule YoutubeEx do
  alias YoutubeEx.LiveComments.Protocol

  @doc "Makes a URL to get the list of channels for a user. Quota cost: 3"
  def make_list_channels_url(api_key, username) do
    "https://www.googleapis.com/youtube/v3/channels?part=contentDetails&key=#{api_key}&forUsername=#{username}"
  end

  @doc "Makes a URL to search for live videos on a given channel. Quota cost: 100"
  def make_list_channel_live_videos_url(api_key, channel_id) do
    "https://www.googleapis.com/youtube/v3/search?part=snippet&key=#{api_key}&channelId=#{channel_id}&eventType=live&type=video"
  end

  @doc "Makes a URL to search for currently live videos, ordered by view count. Quota cost: 100"
  def make_list_live_videos_url(api_key, max_results, page_token \\ nil, category_id \\ nil) do
    if max_results > 50 do
      max_results = 50
    end

    if max_results < 5 do
      max_results = 5
    end

    url = "https://www.googleapis.com/youtube/v3/search?part=snippet&key=#{api_key}&eventType=live&type=video&maxResults=#{max_results}&order=viewCount"

    if category_id do
      url = url <> "&videoCategoryId=#{category_id}"
    end

    if page_token do
      url <> "&pageToken=#{page_token}"
    else
      url
    end
  end

  def make_live_comments_url(video_id, poll_delay, rc, last_timestamp) do
    "https://www.youtube.com/live_comments?action_get_comments=1&video_id=#{video_id}&format=proto&pd=#{poll_delay}&rc=#{rc}&lt=#{last_timestamp}&scr=true&comment_version=1"
  end

  @doc "Makes a URL to retrieve data about a channel. Quota cost: 5"
  def make_channel_info_url(api_key, channel_id) do
    "https://www.googleapis.com/youtube/v3/channels?part=snippet%2CcontentDetails&key=#{api_key}&id=#{channel_id}"
  end

  @doc "Makes a URL to retrieve all data about a live streaming video. Quota cost: 7"
  def make_live_video_details_url(api_key, video_id) do
    "https://www.googleapis.com/youtube/v3/videos?part=snippet,statistics,liveStreamingDetails&key=#{api_key}&id=#{video_id}"
  end

  @doc "Makes a URL to retrieve basic information about a video. Quota cost: 3"
  def make_video_snippet_url(api_key, video_id) do
    "https://www.googleapis.com/youtube/v3/videos?part=snippet&key=#{api_key}&id=#{video_id}"
  end

  def get_user_channels(api_key, username) do
    HTTPotion.get(make_list_channels_url api_key, username).body
    |> Poison.decode!
  end

  def get_live_videos(api_key, max_results, page_token \\ nil) do
    HTTPotion.get(make_list_live_videos_url api_key, max_results, page_token).body
    |> Poison.decode!
  end

  def get_channel_live_videos(api_key, channel_id) do
    HTTPotion.get(make_list_channel_live_videos_url api_key, channel_id).body
    |> Poison.decode!
  end

  def get_video_live_comments(video_id, since_timestamp, poll_delay \\ 2000, rc \\ 0) do
    xml = HTTPotion.get(make_live_comments_url video_id, poll_delay, rc, since_timestamp)
    |> validate_comment_response
    xml = xml.body
    |> Quinn.parse

    latest_time = xml
    |> Quinn.find(:latest_time)
    |> hd
    |> Map.fetch!(:value)
    |> hd

    comment_list = xml
    |> Quinn.find(:comments)
    |> hd
    |> Map.fetch!(:value)
    |> hd
    |> Base.url_decode64!
    |> Protocol.CommentList.decode
    |> Map.from_struct
    |> Dict.put("latest_time", latest_time)

    # Not actually necessary but doesn't break the old API
    Dict.put(comment_list, "poll_delay", comment_list.poll_delay) |>
    Dict.put("comments", comment_list.comments)
  end

  # Raises an error if a live comment response is invalid
  defp validate_comment_response(http_response) do
    if http_response.status_code == 403 do
      # The undocumented API returns 403 for some videos, denying access to their live comments
      raise CommentsNotAccessibleError
    end

    http_response
  end

  def get_live_video_details(api_key, video_id) do
    HTTPotion.get(make_live_video_details_url api_key, video_id).body
    |> Poison.decode!
  end

  def get_video_snippet(api_key, video_id) do
    HTTPotion.get(make_video_snippet_url api_key, video_id).body
    |> Poison.decode!
  end

  def get_channel(api_key, channel_id) do
    HTTPotion.get(make_channel_info_url api_key, channel_id).body
    |> Poison.decode!
  end
end

defmodule CommentsNotAccessibleError do
  defexception message: "forbidden from accessing live comments"
end
