defmodule YoutubeEx.LiveComments.Protocol do
  use Protobuf, """
    message CommentList {
      repeated Comment comments = 1;

      required int64 likes = 2;
      required int64 dislikes = 3;
      required int32 poll_delay = 4;
    }

    message Comment {
      required string comment_id = 1;
      required string comment = 2;
      required int32 is_deleted = 3;
      required int64 time_created = 4;

      required Author author = 8;
    }

    message Author {
      required string id = 1;
      required string profile_image_url = 2;
      required string name = 3;

      required int64 unknown = 4;

      required string channel_id = 11;
    }
  """
end
