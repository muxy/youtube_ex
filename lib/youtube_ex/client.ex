defmodule YoutubeEx.Client do
  use OAuth2.Strategy

  def new(client_id, client_secret, redirect_uri) do
    OAuth2.Client.new([
      strategy: OAuth2.Strategy.AuthCode,
      client_id: client_id,
      client_secret: client_secret,
      site: "https://www.googleapis.com",
      authorize_url: "https://accounts.google.com/o/oauth2/v2/auth",
      token_url: "https://www.googleapis.com/oauth2/v4/token",
      redirect_uri: redirect_uri
    ])
    |> put_param(:access_type, "offline")
    |> put_param(:include_granted_scopes, "true")
  end

  @spec refresh_token!(OAuth2.AccessToken.t) :: OAuth2.AccessToken.t
  def refresh_token!(token) do
    fresh = %{token | client: %{token.client | params: Map.delete(token.client.params, "code")}} # If auth code is still set in client, remove it
    |> OAuth2.AccessToken.refresh!

    %{fresh | refresh_token: token.refresh_token} # Keep the refresh token in the result
  end

  def api_request!(token, url) do
    OAuth2.AccessToken.get! token, url
  end

  def add_param(client, param, value) do
    put_param(client, param, value)
  end

  def authorize_url!(client) do
    OAuth2.Client.authorize_url! client
  end

  def get_token!(client, code) do
    OAuth2.Client.get_token! client, code: code
  end

  @spec expired?(OAuth2.AccessToken.t) :: boolean
  def expired?(token) do
    OAuth2.AccessToken.expired? token
  end
end
