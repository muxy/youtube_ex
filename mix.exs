defmodule YoutubeEx.Mixfile do
  use Mix.Project

  def project do
    [app: :youtube_ex,
     version: "0.0.1",
     deps_path: "../../deps",
     lockfile: "../../mix.lock",
     elixir: "~> 1.0",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps(Mix.env)]
  end

  # Configuration for the OTP application
  #
  # Type `mix help compile.app` for more information
  def application do
    [applications: [:logger, :httpotion, :poison, :oauth2]]
  end

  # Dependencies can be Hex packages:
  #
  #   {:mydep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:mydep, git: "https://github.com/elixir-lang/mydep.git", tag: "0.1.0"}
  #
  # To depend on another app inside the umbrella:
  #
  #   {:myapp, in_umbrella: true}
  #
  # Type `mix help deps` for more examples and options
  defp deps(:prod) do
    [
      {:httpotion, "~> 2.2.0"},
      {:poison, "~> 1.5"},
      {:quinn, "~> 0.0.4"},
      {:exprotobuf, "~> 0.10.0"},
      {:oauth2, "~> 0.5.0"}
    ]
  end

  defp deps(_) do
    [
      {:exvcr, "~> 0.7.0"},
      {:timex, []}
    ] ++ deps(:prod)
  end
end
